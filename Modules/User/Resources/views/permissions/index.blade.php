@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Permissions</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('permission.create') }}"> Create New Permission</a>
        </div>
    </div>
</div>
<br>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
    <tr  class="text-center">
        <th>No</th>
        <th>Name</th>
        <th>Details</th>
        <th>Action</th>
    </tr>
    @foreach ($data['permission'] as $permission)
    <tr class="text-center">
       <td>{{ $permission->id }}</td>
       <td>{{ $permission->name }}</td>
       <td>{{ $permission->guard_name }}</td>
       <td>
        <a class="btn btn-primary" href="{{ route('permission.edit',$permission->id) }}">Edit</a>
        <a class="btn btn-danger" href="{{ route('permission.delete',$permission->id) }}">Delete</a>
    </td>
</tr>
@endforeach
</table>
@endsection